package models;

/**
 * Created by Pasquale on 07/07/2017.
 */
public enum RoleType { ADMIN, USER
}
