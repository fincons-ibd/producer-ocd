export class SearchResult {

    id: string
    keyWords: string[]
    freeText: string
    semanticSearch: string
    inDate: Date
    endDate: Date
    types: string[]

}