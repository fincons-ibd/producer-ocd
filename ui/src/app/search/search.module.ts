import { NgModule }       from '@angular/core';
import { CommonModule }   from '@angular/common';
import { FormsModule }    from '@angular/forms';
import { SearchFormComponent }    from './search-form.component';
import { SearchService } from './search.service';
import { MultimediaContentComponent } from '../multimedia-content/multimedia-content.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { NoopAnimationsModule } from "@angular/platform-browser/animations"
import "hammerjs";
import { MaterialModule, MdMenuModule , MdButtonModule, MdIconModule, MdTabsModule, MdInputModule, MdSelectModule, MdDatepickerModule,  MdNativeDateModule, DateAdapter, MdDateFormats, MdDialog, MdDialogRef } from "@angular/material";
import { FlexLayoutModule } from '@angular/flex-layout';
import { DialogRepositoryComponent, DialogRepositoryDetail } from '../dialog-repository/dialog-repository.component'
import { ProfileComponent } from '../profile/profile.component';
import { DialogDetailComponent,DialogDetail } from "../dialog-detail/dialog-detail.component";
import { BookmarksComponent }    from '../bookmarks/bookmarks.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgxPaginationModule,
    NoopAnimationsModule,
    MaterialModule,
    FlexLayoutModule
  ],
  declarations: [
    SearchFormComponent,
    MultimediaContentComponent,
    ProfileComponent,
    DialogRepositoryComponent,
    DialogRepositoryDetail,
    DialogDetail,
    DialogDetailComponent,
    BookmarksComponent
  ],
  // entryComponents declare Components created manually
  entryComponents: [DialogDetail,DialogRepositoryDetail],  
  providers: [ SearchService ]
})
export class SearchModule {}
