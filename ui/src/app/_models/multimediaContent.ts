export class MultimediaContent {
    type: string
    fileExtension: string
    length: string
    name: string
    description: string
    thumbnail: string
    downloadUri: string
    source: string
    uri: string
    licenseType: string
    date: string
}
